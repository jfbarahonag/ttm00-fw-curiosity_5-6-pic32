/** @file        main.c
 * @author       Juan Felipe Barahona Gonzalez
 * @version      V1.0.20191126
 * @date         2019-11-26
 * @brief        Use of CoreTimer, interrupt on change to activate a state machine that manages a LED pattern. 
 *               UART1 is used for print the states each certain time.
 
 Main page Description
 *  The program uses the CoreTimer and its interruption to activate a LEDs pattern
 *  The program uses UART1 to send the actual state of the states machine.
 */
#include <stdio.h>
#include <stdlib.h>
#include "main.h"

variables_t variable;
state_machine_struct_t sequency_state_machine;
/*
 *  @brief Main function  
 */
int main(void) {
    input_output_init();
    interrupt_setup();
    init_variables();
    sequency_state_machine.evt = EVT_BOOT_OK;
    sequency_state_machine.new_evt = NEW_EVENT;
    while(1) {
        print_state_CT();
        LED_sequency_state_machine();
    }
    return 0;
}
/*
 *  @brief Define pins as a digital input/output  
 */
void input_output_init ( void ) {
    // Initialize LED1, LED2 y LED3
    mPORTESetPinsDigitalOut(BIT_4|BIT_6|BIT_7);
    // Initialize LED RGB
    mPORTBSetPinsDigitalOut(BIT_2|BIT_3|BIT_10);
    // Initialize SW1
    mPORTDSetPinsDigitalIn(BIT_6); 
}
/*
 *  @brief Configure all interrupts  
 */
void interrupt_setup ( void ) {
    //CoreTimer config
    OpenCoreTimer(CORE_TICK_RATE);
    mConfigIntCoreTimer(CT_INT_ON | CT_INT_PRIOR_3 | CT_INT_SUB_PRIOR_0);
    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableInterrupts();
    //Interrupt on change
    mCNDOpen(CONFIG, PINS, PULLUPS);
    ConfigIntCND(INTERRUPT);
    INTEnableSystemMultiVectoredInt();
}
/*
 *  @brief Initial statements 
 */
void init_variables ( void ) {
    /* LEDS initial state */
    LEDS_init();
    /* UART initialized */
    UART1_init(115200); 
    UART1_write_string("Hello UART\n");
    /* First step of machine states */
    sequency_state_machine.evt = EVT_NONE_EVT;
    sequency_state_machine.new_evt = NON_NEW_EVENT;
    sequency_state_machine.state = STATE_BOOT; 
    /* Variables used in state machine */
    variable.led_period = LED_PERIOD;
    variable.uart_period = UPDATE_UART;
    variable.flag_interrupt = 0;
    variable.led_sequence = 0;
}
/*
 *  @brief LEDS start turned off 
 */
void LEDS_init ( void ) {
    LED_off(LED_1);
    LED_off(LED_2);
    LED_off(LED_3); 
    LED_off(LED_R);
    LED_off(LED_G);
    LED_off(LED_B); 
}
/*
 *  @brief Manages the LED and UART times  
 */
void __ISR(_CORE_TIMER_VECTOR, IPL3AUTO) _CoreTimerHandler(void) {
    mCTClearIntFlag();
    UpdateCoreTimer(CORE_TICK_RATE);
    if (variable.flag_interrupt != 0) {
        variable.led_period --;
    } 
    
    if (variable.uart_period != 0) {
        variable.uart_period--; 
    }
       
    if (variable.led_period == 0){
        sequency_state_machine.new_evt = NEW_EVENT;
        sequency_state_machine.evt = EVT_LED_TIMEOUT_EXPIRED;
        variable.led_period = LED_PERIOD;
    } 
}
/*
 *  @brief Interrupt function that activates a new event if previous state is IDLE 
 */
void __ISR(_CHANGE_NOTICE_VECTOR, IPL3AUTO) button_interrupt_Handler(void) { 
    mPORTDReadBits(BIT_6);
    if (sequency_state_machine.state == STATE_IDLE) {
        //sequency_state_machine.state = state_cycling;
        sequency_state_machine.new_evt = NEW_EVENT;
        sequency_state_machine.evt = EVT_BUTTON_PRESS;
    }
    mCNDClearIntFlag();
}
/*
 *  @brief State machine that do the LED pattern
 */
void LED_sequency_state_machine ( void ) {
    if (sequency_state_machine.new_evt) {
        sequency_state_machine.new_evt = NON_NEW_EVENT;
        
        switch (sequency_state_machine.state) {
            
            case STATE_BOOT:
                if (sequency_state_machine.evt == EVT_BOOT_OK) {
                    sequency_state_machine.state = boot_done();                   
                }
                break;
                
            case STATE_IDLE:
                if (sequency_state_machine.evt == EVT_BUTTON_PRESS) {
                    sequency_state_machine.state = after_press_button();        
                }
                break;
                
            case STATE_CYCLING:
                if (sequency_state_machine.evt == EVT_LED_ON_OK) {
                    sequency_state_machine.state = turn_on_LEDS();
                }
                else if (sequency_state_machine.evt == EVT_ALL_LED_ON_OK) {
                    sequency_state_machine.state = return_to_idle();
                }
                break;
                
            case STATE_WAITING:
                if (sequency_state_machine.evt == EVT_LED_TIMEOUT_EXPIRED) {
                    sequency_state_machine.state = return_to_cycling();                    
                }
                break;
                
            default:
                UART1_write_string("Unknown state\n");
                break;
        }
    }
}
/*
 *  @brief if boot was done successfully, go forward to IDLE state
 */
state_t boot_done( void ) {
    return STATE_IDLE;
}
/*
 *  @brief If button was pressed, activates a new event and go forward to CYCLING state
 */
state_t after_press_button( void ) {
    //Start countdown on core interrupt
    sequency_state_machine.new_evt = NEW_EVENT;
    sequency_state_machine.evt = EVT_LED_ON_OK;
    return STATE_CYCLING;
}
/*
 *  @brief Activates core timer countdown by a flag, turns on respective LED and go forward to WAITING state
 */
state_t turn_on_LEDS( void ) {
    //Activate flag to start countdown and turn on led
    variable.flag_interrupt = 1;
    LED_on(variable.led_sequence);
    return STATE_WAITING;
}
/*
 *  @brief Go forward to IDLE state
 */
state_t return_to_idle ( void ) {
    return STATE_IDLE;
}
/*
 *  @brief Compares if all LEDS turn on to return to the IDLE state or continues with LED pattern
 */
state_t return_to_cycling ( void ) {
    //DO SOMETHING HERE
    LED_off(variable.led_sequence);
    if (variable.led_sequence < 2) {  
        variable.led_sequence++;
        sequency_state_machine.evt = EVT_LED_ON_OK;
    } else {
        variable.flag_interrupt = 0;
        sequency_state_machine.evt = EVT_ALL_LED_ON_OK;
        variable.led_sequence = 0;
    }
    sequency_state_machine.new_evt = NEW_EVENT;
    return STATE_CYCLING;
}
/*
 *  @brief Print the state each 300mSecs. 
 */
void print_state_CT( void ) {
    if (variable.uart_period == 0) {
        print_state();
        variable.uart_period = UPDATE_UART;
    }   
}
/*
 *  @brief Select the state for print. 
 */
void print_state ( void ) {
    switch (sequency_state_machine.state) {
        case STATE_BOOT:
            UART1_write_string("BOOT\n");
            break;
        case STATE_IDLE:
            UART1_write_string("IDLE\n");
            break;
        case STATE_CYCLING:
            UART1_write_string("CYCLING\n");
            break;
        case STATE_WAITING:
            UART1_write_string("WAITING\n");
            break;
        default:
            break;
    } 
}