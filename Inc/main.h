/* 
 * File:   main.h
 * Author: user19
 *
 * Created on November 22, 2019, 2:49 PM
 */

#ifndef MAIN_H
#define	MAIN_H
#include "config.h"
#include "pic32_LEDS.h"
#include "pic32_uart.h"

#define FW_VER "1.0"
#define AUTHOR "FelipeBarahona"
//Timers configurations
#define SYS_FREQ        (96000000UL)
#define PBCLK_FREQUENCY        (96 * 1000 * 1000)
#define TICKS_PER_SEC   1000
#define CORE_TICK_RATE  (SYS_FREQ/2/TICKS_PER_SEC)
 //Change notice configurations
#define CONFIG          (CND_ON | CND_IDLE_CON)
#define PINS            (CND6_ENABLE)
#define PULLUPS         (CND_PULLUP_DISABLE_ALL)
#define INTERRUPT       (CHANGE_INT_PRI_2 | CHANGE_INT_ON)
//Times
#define LED_PERIOD      1000
#define UPDATE_UART     300
//Events constants
#define NEW_EVENT       1
#define NON_NEW_EVENT   0

//States
typedef enum{
    STATE_BOOT,
    STATE_IDLE,
    STATE_CYCLING,
    STATE_WAITING
}state_t;

//Events
typedef enum{
    EVT_NONE_EVT,
    EVT_BOOT_OK,
    EVT_BUTTON_PRESS,
    EVT_LED_ON_OK,
    EVT_LED_TIMEOUT_EXPIRED,
    EVT_ALL_LED_ON_OK       
}evt_t;

//State machine structure
typedef volatile struct {
    uint8_t state;
    uint8_t evt;
    uint8_t new_evt;
}state_machine_struct_t;
// Variables used in state machine structure
typedef volatile struct {
    uint8_t  flag_interrupt;
    uint8_t  led_sequence;
    uint16_t led_period;
    uint16_t uart_period;
}variables_t;

void input_output_init ( void );
void interrupt_setup ( void );
void init_variables ( void );
void LEDS_init ( void );
void LED_sequency_state_machine ( void );

state_t boot_done( void );
state_t after_press_button( void );
state_t turn_on_LEDS( void );
state_t return_to_idle ( void );
state_t return_to_cycling ( void );

void print_state_CT ( void );
void print_state ( void );

#endif	/* MAIN_H */

